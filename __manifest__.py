{
    "name": "CG SCOP - Import de données",
    "summary": "CG SCOP - Import de données",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "cgscop_partner",
        "cgscop_tantiemo",
    ],
    "data": [
        "views/scop_import_views.xml",
    ]
}
